<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">

        <!-- <?php if($auth) {?> -->
        <!-- <li class="heading"><?= __('Actions') ?></li> -->
<!--         <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Likes'), ['controller' => 'Likes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Like'), ['controller' => 'Likes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Posts'), ['controller' => 'Posts', 'action' => 'index']) ?></li> -->
        <h5><?= $this->Html->link(__('Followings'), ['controller' => 'Users', 'action' => 'followings']) ?></h5>
        <!-- <li><?= $this->Html->link(__('Edit Profile'), ['action' => 'edit', $auth['User']['id']]) ?></li>
        <li><?php echo ('Welcome to your profile'); ?> -->
        <br>
    

                
                  
                    <!-- <?php } ?>  -->
 

    </ul>
</nav>
<!-- <div class="col-md-6">
    <form action="<?php echo $this->Url->build(['action' => 'followers'])?>" method="get">
        <div class= "input=group">
          <br><br>
            <input type="search" name="q" class="form-control">
            <div class="input-group-prepend">
                <button class="input-group-primary input-group-text" type="submit">submit</button>
            </div>
        </div> 
        
    </form>
    
</div> -->
<div class="users index large-9 medium-8 columns content">
    <h3><?= __('List of Followers') ?></h3>
    <table cellpadding="0" cellspacing="0">
        
        <thead>
            <tr>
                <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
                <th scope="col"><?= $this->Paginator->sort('username') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email') ?></th>
<!--                 <th scope="col"><?= $this->Paginator->sort('password') ?></th>
                <th scope="col"><?= $this->Paginator->sort('bio') ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_activated') ?></th>
                <th scope="col"><?= $this->Paginator->sort('image') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th> -->
                <th scope="col" class="actions"><?= __('') ?></th> 
            </tr>   
        </thead>
               <?php foreach ($userfollowers as $users): ?>
          <tr>
                 <!-- <td><?= h($users->id) ?></td> -->
                 <td><?= h($users->username) ?></td>
                 <td><?= h($users->email) ?></td>
                 <!-- <td><br><?= h($users->username) ?></td> -->
                <!-- <td><?= $this->Html->link(__('View'), ['action' => 'view', $users->id]) ?> </td> -->  
             </tr>
              <?php endforeach; ?>
        
    </table>
     <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div> 
</div>


<!-- asdas -->


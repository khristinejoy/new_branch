<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<!-- <nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Log In'), ['action' => 'login']) ?></li>
        <li><?= $this->Html->link(__('List Likes'), ['controller' => 'Likes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Like'), ['controller' => 'Likes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Posts'), ['controller' => 'Posts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Post'), ['controller' => 'Posts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Retweets'), ['controller' => 'Retweets', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Retweet'), ['controller' => 'Retweets', 'action' => 'add']) ?></li>
    </ul>
</nav> -->
<div class="users form large-9 medium-8 columns content">
    <?= $this->Flash->render() ?>
    <?= $this->Form->create($user);?>
    <fieldset>
        <legend><?= __('Sign Up') ?></legend>
        <?php
            echo $this->Form->control('username');
            echo $this->Form->control('email');
            echo $this->Form->control('password');
            echo $this->Form->control('confirm_password', ['type'=> 'password']);
            echo $this->Form->control('bio');
            echo $this->Form->hidden('is_activated');
            echo $this->Form->hidden('token');
            // echo $this->Form->control('image', ['type'=> 'file']);
          // echo $this->Form->input('upload', array('type' => 'file'));
         ?>

          <input type="file" accept="image/*" name="image" onchange="loadFile(event)">
         <img id="output"/>
        <script>
          var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
          };
        </script>
 

    </fieldset>
         <?= $this->Form->button(__('Sign Up'), ['class'=>'btn btn-primary']) ?>

    <?= $this->Form->end() ?>
</div>



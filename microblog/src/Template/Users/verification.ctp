<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="users form large-9 medium-8 columns content">

<div class>
<h3>Your email has been verified, please log in now</h3>
<?php echo $this->Html->link('Login here', ['action'=>'login']); ?>

</div>
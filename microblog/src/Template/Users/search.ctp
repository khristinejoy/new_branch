<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">

        <!-- <?php if($auth) {?> -->
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Likes'), ['controller' => 'Likes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Like'), ['controller' => 'Likes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Posts'), ['controller' => 'Posts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Post'), ['controller' => 'Posts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Retweets'), ['controller' => 'Retweets', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Edit Profile'), ['action' => 'edit', $auth['User']['id']]) ?></li>
        <li><?php echo ('Welcome to your profile'); ?>
                 <!-- <li><?php echo ('Id: '), $auth['User']['id']; ?>
                </li>  -->
                 <li><?php echo ('Username: '), $auth['User']['username']; ?>
                </li>                
                <li><?php echo ('Email: '), $auth['User']['email']; ?>
                </li>               
                <li><?php echo ('Bio: '), $auth['User']['bio']; ?>
                </li>
                <li><?php echo ('Image: '), $auth['User']['image']; ?>
                </li>  

                
                  
                    <!-- <?php } ?>  -->
 

    </ul>
</nav>
<div class="col-md-6">
    <form action="<?php echo $this->Url->build(['action' => 'search'])?>" method="get">
        <div class= "input=group">
            <input type="search" name="q" class="form-control">
            <div class="input-group-prepend">
                <button class="input-group-primary input-group-text" type="submit">submit</button>
            </div>
        </div> 
        
    </form>
    
</div>
<div class="users index large-9 medium-8 columns content">
    <h3><?= __('User Profile') ?></h3>
    <table cellpadding="0" cellspacing="0">
        
        <thead>
            <tr>
                <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
                <th scope="col"><?= $this->Paginator->sort('username') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email') ?></th>
               <!--  <th scope="col"><?= $this->Paginator->sort('password') ?></th>
                <th scope="col"><?= $this->Paginator->sort('bio') ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_activated') ?></th>
                <th scope="col"><?= $this->Paginator->sort('image') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
 -->            </tr>   
        </thead>
         <?php foreach ($users as $users): ?>
            <tr>
                 <td><?= h($users->username) ?></td>
                 <td><?= h($users->email) ?></td>
             </tr>
                      <?php endforeach; ?>

    
    </table>
     <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div> 
</div>


<!-- asdas -->


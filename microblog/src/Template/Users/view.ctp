<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
   <!--  <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User'), ['action' => 'edit', $user->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Likes'), ['controller' => 'Likes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Like'), ['controller' => 'Likes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Posts'), ['controller' => 'Posts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Post'), ['controller' => 'Posts', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Retweets'), ['controller' => 'Retweets', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Retweet'), ['controller' => 'Retweets', 'action' => 'add']) ?> </li> -->
               
              <!--  <fieldset>
                <h5><?php echo ('Followers: '), $totalfollowers ?></h5>
                <h5><?php echo ('Followings: '), $totalfollowings ?></h5>
                </fieldset> -->
                

    </ul>
</nav>

<div class="users view large-9 medium-8 columns content" align="center">
    <!-- <h3><?= h($user->id) ?></h3> -->
    <h3><?= h($user->username) ?>  </h3>

             <?= $this->Form->create();?>

                <?php if(empty($userfollow))
                {
                    echo  "<h5>".$this->Form->submit(__('Follow'), ['name'=>'submit','class'=>'btn btn-primary'])."</h5>";

                }else{
                     echo  "<h5>".$this->Form->submit(__('Unfollow'), ['name'=>'submit','class'=>'btn btn-primary'])."</h5>";

                }

                ?>
            
              <?= $this->Form->end() ?>

 




    <table class="vertical-table">
<!--         <tr>
            <th scope="row"><?= __('Username') ?></th>
            <td><?= h($user->username) ?></td>
        </tr> -->
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($user->email) ?></td>
        </tr>
      <!--   <tr>
            <th scope="row"><?= __('Password') ?></th>
            <td><?= h($user->password) ?></td>
        </tr> -->
        <tr>
            <th scope="row"><?= __('Bio') ?></th>
            <td><?= h($user->bio) ?></td>
        </tr>
         <tr>
            <th scope="row"><?= __('Image') ?></th>
            <td><?= h($user->image) ?></td>
        </tr>


<!--         <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($user->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Activated') ?></th>
            <td><?= $this->Number->format($user->is_activated) ?></td>
        </tr> -->
         <!--        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($user->created) ?></td>
        </tr> -->
    </table>
     <fieldset>
                <h5><?php echo ('Followers: '), $totalfollowers ?></h5>
                <h5><?php echo ('Followings: '), $totalfollowings ?></h5>
                </fieldset>
    <!-- <div class="related">
        <h4><?= __('Related Likes') ?></h4>
        <?php if (!empty($user->likes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Post Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->likes as $likes): ?>
            <tr>
                <td><?= h($likes->id) ?></td>
                <td><?= h($likes->user_id) ?></td>
                <td><?= h($likes->post_id) ?></td>
                <td><?= h($likes->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Likes', 'action' => 'view', $likes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Likes', 'action' => 'edit', $likes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Likes', 'action' => 'delete', $likes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $likes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Posts') ?></h4>
        <?php if (!empty($user->posts)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Post') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->posts as $posts): ?>
            <tr>
                <td><?= h($posts->id) ?></td>
                <td><?= h($posts->user_id) ?></td>
                <td><?= h($posts->post) ?></td>
                <td><?= h($posts->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Posts', 'action' => 'view', $posts->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Posts', 'action' => 'edit', $posts->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Posts', 'action' => 'delete', $posts->id], ['confirm' => __('Are you sure you want to delete # {0}?', $posts->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Retweets') ?></h4>
        <?php if (!empty($user->retweets)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Post Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->retweets as $retweets): ?>
            <tr>
                <td><?= h($retweets->id) ?></td>
                <td><?= h($retweets->user_id) ?></td>
                <td><?= h($retweets->post_id) ?></td>
                <td><?= h($retweets->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Retweets', 'action' => 'view', $retweets->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Retweets', 'action' => 'edit', $retweets->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Retweets', 'action' => 'delete', $retweets->id], ['confirm' => __('Are you sure you want to delete # {0}?', $retweets->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div> -->
</div>

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<head>
<!-- <style type="text/css">
    body {
    background: -webkit-linear-gradient(90deg, #1BD8D0 10%, #125DEB 90%);
    background: -moz-linear-gradient(90deg, #1BD8D0 10%, #125DEB 90%);
    background: -ms-linear-gradient(90deg, #1BD8D0 10%, #125DEB 90%);
    background: -o-linear-gradient(90deg, #1BD8D0 10%, #125DEB 90%);
    background: linear-gradient(90deg, #1BD8D0 10%, #125DEB 90%);
    }

</style> -->
</head>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <!-- <li class="heading"><?= __('Actions') ?></li> -->
        <fieldset>
        <h3><?= $this->Html->link(__('Sign Up'), ['action' => 'signup']) ?></h3>
        </fieldset>
        <!-- <li><?= $this->Html->link(__('List Likes'), ['controller' => 'Likes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Like'), ['controller' => 'Likes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Posts'), ['controller' => 'Posts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Post'), ['controller' => 'Posts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Retweets'), ['controller' => 'Retweets', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Retweet'), ['controller' => 'Retweets', 'action' => 'add']) ?></li> -->
        
    </ul>
</nav>
 <div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create() ?>
    <fieldset>
        <legend><?= __('Log In') ?></legend>
        <?php
            echo $this->Form->control('email') ;
            echo $this->Form->control('password');
        ?>
    </fieldset>
     <?= $this->Form->button(__('Sign Up'), ['class'=>'btn btn-primary']) ?>
    <?= $this->Form->end() ?>
</div> 


<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Retweet $retweet
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Retweet'), ['action' => 'edit', $retweet->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Retweet'), ['action' => 'delete', $retweet->id], ['confirm' => __('Are you sure you want to delete # {0}?', $retweet->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Retweets'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Retweet'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Posts'), ['controller' => 'Posts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Post'), ['controller' => 'Posts', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="retweets view large-9 medium-8 columns content">
    <h3><?= h($retweet->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $retweet->has('user') ? $this->Html->link($retweet->user->id, ['controller' => 'Users', 'action' => 'view', $retweet->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Post') ?></th>
            <td><?= $retweet->has('post') ? $this->Html->link($retweet->post->id, ['controller' => 'Posts', 'action' => 'view', $retweet->post->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($retweet->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($retweet->created) ?></td>
        </tr>
    </table>
</div>

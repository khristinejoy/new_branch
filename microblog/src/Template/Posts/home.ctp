<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Post $post
 */
?>
<!-- <style type="text/css">
 table {
    width:70%; 
    margin-left:25%; 
    margin-right:15%;
  }
</style> -->
<!-- <nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Posts'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Likes'), ['controller' => 'Likes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Like'), ['controller' => 'Likes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Retweets'), ['controller' => 'Retweets', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Retweet'), ['controller' => 'Retweets', 'action' => 'add']) ?></li>
    </ul>
</nav> -->

    <div class="col-md-3">
    <form action="<?php echo $this->Url->build(['action' => 'home'])?>" method="get">
        <br>
        <div class= "input=group">

            <input type="search" name="q" class="form-control">
            <div class="input-group-prepend">
                <button class="btn btn-primary" type="submit">Search Post</button>
            </div>
        </div> 
        
    </form>
</div>

 <div class="posts form large-7 medium-12 columns content">
    <?= $this->Form->create($post) ?>
    <fieldset>
        <legend><?= __('Add Post') ?></legend>
        <?php
            // echo $this->Form->control('user_id', ['options' => $users]);
            // echo $this->Form->control('post');
         // echo $this->Form->input('title');
        echo $this->Form->input('post', ['rows' => '3', array('maxlength'=>'140', 'type' => 'text')]);
        
        ?>
    </fieldset>
    <?= $this->Form->button(__('Add Post'),['class'=>'btn btn-primary']) ?>
    <?= $this->Form->end() ?> 
</div>

       <!-- <?= $this->Form->create();?> -->
<div class="posts index large-9 medium-12 columns content" align="center">
    

    <h3><?= __('Posts') ?></h3>
    <table cellpadding="0" cellspacing="0" align="left" >
 <!--        <col width="170">
        <col width="380"> -->
        <thead>
            <tr>
                <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
                <!-- <th scope="col"><?= $this->Paginator->sort('user_id') ?></th> -->
                <!-- <th scope="col"><?= $this->Paginator->sort('post') ?></th> -->
                <!-- <th scope="col"><?= $this->Paginator->sort('created') ?></th> -->
                <!-- <th scope="col" class="actions"><?= __('Actions') ?></th> -->
            </tr>
        </thead>
        <tbody>
            <?php foreach ($posts as $post): ?>
            <tr>
                <!-- <td><?= $this->Number->format($post->id) ?></td> -->
                <td><?= $post->has('user') ? $this->Html->link($post->user->username, ['controller' => 'Users', 'action' => 'view', $post->user->id]) : '' ?></td>
                <td><br><br><?= h($post->post) ?></td>

                <td><?= h($post->created) ?></td> 
                <td>
                       <?php if (empty($likesTable)){ ?>

                             <?= $this->Html->link(__('Like'), ['action' => 'like', $post->id]) ?>

                           <!-- echo  $this->Html->link(__('Like'), ['action' => 'like', $post->id]);                  -->
                            <?php } else{ ?>

                         <?= $this->Html->link(__('Unlike'), ['action' => 'like', $post->id]) ?>



                            <!-- echo  $this->Html->link(__('Unlike'), ['action' => 'like', $post->id]);                  -->

         
                           <?php } ?>
                            
                  <button class="btn btn-primary">Retweet</button></td>
                <!-- <td class="actions"> -->
                    <!-- <?= $this->Html->link(__('View'), ['action' => 'view', $post->id]) ?> -->
                    <!-- <?= $this->Html->link(__('Edit'), ['action' => 'edit', $post->id]) ?> -->
                    <!-- <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $post->id], ['confirm' => __('Are you sure you want to delete # {0}?', $post->id)]) ?> -->
                <!-- </td> -->
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
       <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div> 

    </div>
                  <!-- <?= $this->Form->end() ?> -->


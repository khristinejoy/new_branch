<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Like[]|\Cake\Collection\CollectionInterface $likes
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
<!--         <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Likes'), ['controller' => 'Likes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Like'), ['controller' => 'Likes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Posts'), ['controller' => 'Posts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Post'), ['controller' => 'Posts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Retweets'), ['controller' => 'Retweets', 'action' => 'index']) ?></li> -->
       </li>
         <h3><?php echo ('Welcome '), $auth['User']['username']; ?>
         <br>
                <li><?php echo ('Username: '), $auth['User']['username']; ?>
                </li>                
                <!-- <th scope="row"><?= __('Username') ?></th> -->
                <!-- <td><?= h($auth->username) ?></td> -->
                <li><?php echo ('Email: '), $auth['User']['email']; ?>
                </li>               
                <li><?php echo ('Bio: '), $auth['User']['bio']; ?>
                </li>
                <li><?php echo ('Image: '), $auth['User']['image']; ?>
                </li>  
         <li><?= $this->Html->link(__('Edit Profile'), ['controller' => 'Users','action' => 'edit', $auth['User']['id']]) ?>       <li><h5><?php echo ('Followers: '), $postsfollowers ?></h5>
                </li>  
                <li><h5><?php echo ('Followings: '), $postsfollowing ?></h5>
                </li>  
    </ul>
</nav>

<div class="posts index large-9 medium-8 columns content">
    

    <h3><?= __('Your Posts') ?></h3> 
    <!-- <h3><?php echo $auth['User']['username'], ("'s Posts"); ?></h3> -->

    <br><br>
    <table cellpadding="0" cellspacing="0">
          <col width="460">
          <col width="130">

        <!-- <thead>
            <tr>
                 <th scope="col"><?= $this->Paginator->sort('id') ?></th> 
                 <th scope="col"><?= $this->Paginator->sort('user_id') ?></th> 
                 <th scope="col"><?= $this->Paginator->sort('post') ?></th> 
                 <th scope="col"><?= $this->Paginator->sort('created') ?></th> 
                 <th scope="col" class="actions"><?= __('Actions') ?></th> 
            </tr>
        </thead>
 -->
         <?php foreach ($posts as $post): ?>
    <tr>
                 <td><br><br><?= h($post->post) ?></td>
                 
                 <td><?= h($post->created) ?></td>


        <td>
            <?php
                echo $this->Html->link(
                    $post['Posts']['post'],
                    array('controller'=>'Posts','action' => 'view', $post['Posts']['id'])
                );
            ?>
        </td>
        <td>
            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $post->id]) ?>
            <br>
            <br>
            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $post->id], ['confirm' => __('Are you sure you want to delete # {0}?', $post->id)]) ?>
        </td>
        <td>
            <?php echo $post['Posts']['created']; ?>
        </td>
    </tr>
    <?php endforeach; ?>

       </table>

    </div>
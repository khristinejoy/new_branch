<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Follower $follower
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Follower'), ['action' => 'edit', $follower->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Follower'), ['action' => 'delete', $follower->id], ['confirm' => __('Are you sure you want to delete # {0}?', $follower->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Followers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Follower'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="followers view large-9 medium-8 columns content">
    <h3><?= h($follower->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($follower->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Followerid') ?></th>
            <td><?= $this->Number->format($follower->user_followerid) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Followedid') ?></th>
            <td><?= $this->Number->format($follower->user_followedid) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($follower->created) ?></td>
        </tr>
    </table>
</div>

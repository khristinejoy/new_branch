<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;



/**
 * Posts Controller
 *
 * @property \App\Model\Table\PostsTable $Posts
 *
 * @method \App\Model\Entity\Post[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PostsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */



    public function index()
    {
       $this->set('posts', $this->Posts->find('all'));
       $user = $this->Posts->get($id, [
            'contain' => ['Likes', 'Users', 'Retweets']
        ]);

        $this->paginate = [
            'contain' => ['Users']
        ];
        $posts = $this->paginate($this->Posts);

        $this->set(compact('posts'));

            }

    /**
     * View method
     *
     * @param string|null $id Post id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $post = $this->Posts->get($id, [
            'contain' => ['Users', 'Likes', 'Retweets']
        ]);

        $this->set('post', $post);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
        public function profile()
    {
            //will display users post only
        $this->set('posts', $this->Posts->find('all', array(
                                'conditions' => array('Posts.user_id' => $this->Auth->user('id')))));

        $this->set('posts', $this->Posts->find('all', array(
                                'conditions' => array('Posts.user_id' => $this->Auth->user('id')))));

        $followersTable = TableRegistry::get('Followers');
                     $totalfollowings = $followersTable->find()->where(['user_follower_id'=> $this->Auth->user('id')])->count();
                            // $this->Flash->success(__($totalfollowings));
                            $this->set('postsfollowing',$totalfollowings);

                    $totalfollowers =  $followersTable->find()->where(['user_followed_id'=> $this->Auth->user('id')])->count();
                            // $this->Flash->success(__($totalfollowers));
                        $this->set('postsfollowers',$totalfollowers);





            
        
    }

    public function home($id = null)
    {
         

        $post = $this->Posts->newEntity();
        if ($this->request->is('post')) {

             $user = $this->Auth->user('id');
             $mypost = $this->request->getData('post');
             $mycreated = date('Y-m-d H:i:s');
             $post->user_id = $user;
             $post->post = $mypost;
             $post->created = $mycreated;
            // $post = $this->Posts->patchEntity($post, $this->request->getData());
            if ($this->Posts->save($post)) {
                $this->Flash->success(__('Successfully Posted.'));

                return $this->redirect(['controller'=> 'Posts', 'action' => 'index']);
            }
            $this->Flash->error(__('The post could not be posted. Please, try again.'));
        }
        $users = $this->Posts->Users->find('list', ['limit' => 200]);
        // $posts = $this->paginate($this->Posts);

        $this->set(compact('post', 'users'));



        $this->paginate = [
            'contain' => ['Users']
        ];
        $posts = $this->paginate($this->Posts);

        $this->set(compact('posts'));

        // search for posts in home page / users that you follow
        $search = $this->request->getQuery('q');
            $posta = $this->paginate($this->Posts->find()->where(function($exp, $query) use($search){
            return $exp->like('post','%'.$search.'%');
        }));
              $this->set('posts',$posta);

         //display in desc order posts
          

       
    }

    public function like($id = null){
       $post = $this->Posts->get($id, [
            'contain' => ['Users', 'Likes', 'Retweets']
        ]);

                        $this->Flash->success(__('Working'));
                        $this->Flash->success(__($id));

                            $likesTable = TableRegistry::get('Likes');
                            $query = $likesTable->find();
                            $likesTable = $likesTable
                            ->find()
                            ->select('id')
                            ->where(['user_id' => $this->Auth->user('id')])
                            ->where(['post_id' => $id])
                            ->first();
                             $this->Flash->success(__($likesTable));

                             if (is_null($likesTable)) {
                                   $likesTable = TableRegistry::get('Likes');
                                    $query = $likesTable->newEntity();
                                    $query->user_id = $this->Auth->user('id');
                                    $query->post_id = $id;
                                    $query->created = date('Y-m-d H:i:s');

                                    if ($likesTable->save($query)){
                                        $this->Flash->success(__('The post has been successfully liked.'));
                                                return $this->redirect(['action' => 'home']);

                                    }
                                    else {
                                        $this->Flash->error(__('Something went wrong. Please try again.'));
                                    }
                                }
                            // }
                                 else if (!is_null($likesTable)) {                
                                $likesTable = TableRegistry::get('Likes');
                                $query = $likesTable->find();
                                $likesTable = $likesTable
                                ->find()
                                ->select('id')
                                ->where(['user_id' => $this->Auth->user('id')])
                                ->where(['post_id' => $id])
                                ->first();
                                    if ($this->Posts->Likes->delete($likesTable)) {
                                         $this->Flash->success(__('The post has been successfully unliked.'));
                                         // $this->set('userfollow', $followersTable);
                                          // return $this->redirect(['action' => 'home']);
   
                                          }
                                          else {
                                         $this->Flash->error(__('Something went wrong. Please try again.'));

                                          }    
                                        } 
                                                    $this->set('likesTable', $likesTable);


    }


    /**
     * Edit method
     *
     * @param string|null $id Post id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */

    public function addpost(){
        $post = $this->Posts->newEntity();
        if ($this->request->is('post')) {

             $user = $this->Auth->user('id');
             $mypost = $this->request->getData('post');
             $mycreated = date('Y-m-d H:i:s');
             $post->user_id = $user;
             $post->post = $mypost;
             $post->created = $mycreated;
            // $post = $this->Posts->patchEntity($post, $this->request->getData());
            if ($this->Posts->save($post)) {
                $this->Flash->success(__('The post has been saved.'));

                return $this->redirect(['controller'=> 'Posts', 'action' => 'index']);
            }
            $this->Flash->error(__('The post could not be saved. Please, try again.'));
        }
        $users = $this->Posts->Users->find('list', ['limit' => 200]);
        $this->set(compact('post', 'users'));


    }

    public function edit($id = null)
    {
        $post = $this->Posts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $post = $this->Posts->patchEntity($post, $this->request->getData());
            if ($this->Posts->save($post)) {
                $this->Flash->success(__('The post has been saved.'));

                return $this->redirect(['action' => 'home', $id]);
            }
            $this->Flash->error(__('The post could not be saved. Please, try again.'));
        }
        $users = $this->Posts->Users->find('list', ['limit' => 200]);
        $this->set(compact('post', 'users'));

        // if (empty($this->data)) {
        //     $this->data = $this->Posts->read(NULL, $id);
        // }else{
        //     if ($this->Posts->save($this->data)) {
        //         $this->Flash->success(__('The post has been updated.'));
        //         $this->redirect(array('action'=>'view',$id));
        //     }
        // }
    }

    /**
     * Delete method
     *
     * @param string|null $id Post id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $post = $this->Posts->get($id);
        if ($this->Posts->delete($post)) {
            $this->Flash->success(__('The post has been deleted.'));
        } else {
            $this->Flash->error(__('The post could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}

<?php
namespace App\Controller;


use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Auth\DefaultPasswordHasher;
// use Cake\Validation\Validator;
use Cake\Utility\Security;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;
use Cake\Routing\Router;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    public function beforeFilter(Event $event)
    {
        $this->Auth->allow(['signup', 'forgotPassword', 'verification']);
        // $this->Auth->deny(['index']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function intialize()
    {
        parent::initialize();
        // $this->loadComponent('Upload');
        $this->loadModel('Users');
    }

    public function index()
    {


        // $Usertable = TableRegistry::get('Users');
        // $query =$Usertable->find();
        // $Usertable = $Usertable
        // ->find()
        // ->select(['id','username','created'])
        // ->where(['id !=' =>  $this->Auth->user('id')])
        // ->order(['created' => 'DESC']);
        // // $this->set('table',$Usertable);
        //     // $query = $this->Users->find()
        //     $this->set('table',$this->paginate($Usertable));
        
        $users = $this->paginate($this->Users);
        // $this->set('users', $this->Users->find('all', array(
        //                         'conditions' => array('Users.id ' => $this->Auth->user('id')))));]

                $search = $this->request->getQuery('q');
         $this->paginate = [
            'limit' => '20'];

            $usersa = $this->paginate($this->Users->find()->where(function($exp, $query) use($search){
            return $exp->like('username','%'.$search.'%' );
                         $this->set('users',$usersa);

        }));


        //na aarange nya in desc but not naka paginate






                // $this->set(compact('users'));

           

        $this->set('users',$users);
       
    }

    public function search(){
        $search = $this->request->getQuery('q');
         $this->paginate = [
            'limit' => '15'];

            $usersa = $this->paginate($this->Users->find()->where(function($exp, $query) use($search){
            return $exp->like('username','%'.$search.'%' );
        }));
              $this->set('users',$usersa);

        }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function allusers(){
      // $users = $this->paginate($this->Users);
      //            $this->set('users',$users);

                //display all users na di pa nya nafofollow
        
                  // $this->Users->find('all', array('joins' => array(array('table'=> 'followers',
                  //               'alias' => 'Followers',
                  //               'type' => 'INNER',
                  //               'conditions' => array('NOT' => array('Users.id ' => 'Followers.user_follower_id')

                  //               )))));

      $search = $this->request->getQuery('q');
         $this->paginate = [
            'limit' => '15'];

            $usersa = $this->paginate($this->Users->find()->where(function($exp, $query) use($search){
            return $exp->like('username','%'.$search.'%' );
        }));
              $this->set('users',$usersa);    
    }

    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Likes', 'Posts', 'Retweets', 'Followers']
        ]);


        //
             if ($user) {
                            $followersTable = TableRegistry::get('Followers');
                            // $follower = $this->request->getData('user_follower_id');
                            // $followed = $this->request->getData('user_followed_id');                            
                            // $user = $this->Users->patchEntity($user, $this->request->getData());
                            $query = $followersTable->find();
                            $followersTable = $followersTable
                            ->find()
                            ->select('id')
                            ->where(['user_follower_id' => $this->Auth->user('id')])
                            ->where(['user_followed_id' => $id])
                            ->first();
                            if ($this->request->is('post') && $this->request->getData('submit') === 'Unfollow'){
                                if (!is_null($followersTable)) {
                                $followersTable = TableRegistry::get('Followers');
                                $query = $followersTable->find();
                                $followersTable = $followersTable
                                ->find()
                                ->select('id')
                                ->where(['user_follower_id' => $this->Auth->user('id')])
                                ->where(['user_followed_id' => $id])
                                ->first();
                                    if ($this->Users->Followers->delete($followersTable)) {
                                         $this->Flash->success(__('The user has been successfully unfollowed.'));
                                         // $this->set('userfollow', $followersTable);

                                          }
                                          else{
                                         $this->Flash->error(__('Something went wrong. Please try again.'));

                                          }
                            
                                        }                                                                                       
                                }
                                else if ($this->request->is('post') && $this->request->getData('submit') === 'Follow') {
                                        if (is_null($followersTable)){
                                    $followersTable = TableRegistry::get('Followers');
                                    $query = $followersTable->newEntity();
                                    $query->user_follower_id = $this->Auth->user('id');
                                    $query->user_followed_id = $id;
                                    $query->created = date('Y-m-d H:i:s');

                                    if ($followersTable->save($query)){
                                        $this->Flash->success(__('The users has been successfully followed.'));
                                    }else{
                                        $this->Flash->error(__('Something went wrong. Please try again.'));
                                    }

                                }
                            }

                        }

          $this->set('userfollow', $followersTable);

                        // no of followers
                    $followersTable = TableRegistry::get('Followers');
                     $totalfollowings = $followersTable->find()->where(['user_follower_id'=> $id])->count();
                            // $this->Flash->success(__($totalfollowings));
                            // $this->set('viewfollowings',$totalfollowings);
                        $this->set(compact('totalfollowings',$totalfollowings));


                    $totalfollowers =  $followersTable->find()->where(['user_followed_id'=> $id])->count();
                            // $this->Flash->success(__($totalfollowers));
                        // $this->set('viewfollowers',$totalfollowers);
                                    $this->set(compact('totalfollowers',$totalfollowers));


        // $this->set(compact('user'));
       $this->set('user', $user);




    }

    public function followers(){
       $users = $this->paginate($this->Users);
        // $this->set('users', $this->Users->find('all', array(
        //                         'conditions' => array('Users.id' => $this->Auth->user('id')))));

       $followersTable = TableRegistry::get('Followers');
                            $query = $followersTable->find();
                            $followersTable = $followersTable
                            ->find()
                            ->select('user_follower_id')
                            // ->type('INNER')
                            ->where(['user_followed_id ' => $this->Auth->user('id')]);
                            // ->where(['user_followed_id' => $this->Auth->user('id')]);
                         // $this->set('userfollowers', $followersTable);

        $userDetails = TableRegistry::get('Users');
                            $query = $userDetails->find();
                            $userDetails = $userDetails
                            ->find()
                            ->select(['id', 'username', 'email'])
                            // ->type('INNER')
                            ->where(['users.id IN' => $followersTable]);
                            $this->set('userfollowers', $userDetails);
    }
    
    public function followings(){
       $users = $this->paginate($this->Users);
        // $this->set('users', $this->Users->find('all', array(
        //                         'conditions' => array('Users.id' => $this->Auth->user('id')))));

       $followersTable = TableRegistry::get('Followers');
                            $query = $followersTable->find();
                            $followersTable = $followersTable
                            ->find()
                            ->select('user_followed_id')
                            // ->type('INNER')
                            ->where(['user_follower_id ' => $this->Auth->user('id')]);
                            // ->where(['user_followed_id' => $this->Auth->user('id')]);
                         // $this->set('userfollowers', $followersTable);

        $userDetails = TableRegistry::get('Users');
                            $query = $userDetails->find();
                            $userDetails = $userDetails
                            ->find()
                            ->select(['id', 'username', 'email'])
                            // ->type('INNER')
                            ->where(['users.id IN' => $followersTable]);
                            $this->set('userfollwings', $userDetails);
    }
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));

    }

    public function login()
        {
            //login function


                  if($this->Auth->user('id')){  // check if user is logged in already
                                $this->Flash->warning(__('You are already Logged In'));
                                // return $this->redirect(['controller' => 'Users', 'action' => 'index']);
                        return $this->redirect(['controller' => 'Posts']);
                                      
                       }

                if ($this->request->is('post')) { // check if login form is submitted
            
                        // if user not logged in, attempts to logged user in
                           $user = $this->Auth->identify();
                           if ($user) {
                            $userTable = TableRegistry::get('Users');
                            $uemail = $this->request->getData('email');
                            // $user = $this->Users->patchEntity($user, $this->request->getData());
                            $query = $userTable->find();
                            $userTable = $userTable
                            ->find()
                            ->where(['email' => $uemail])
                            ->where(['is_activated' => 1])
                            ->first();
                            if (!is_null($userTable)) {
                                $this->Auth->setUser($user);
                               //redirect
                               $this->Flash->success(__('Successfully Logged In'));
                               // return $this->redirect(['controller' => 'Users', 'action' => 'index']);
                             return $this->redirect(['controller' => 'Posts', 'action' => 'home']);
                            
                            } else{
                                return $this->redirect(['controller' => 'Posts', 'action' => 'home']);
                                $this->Flash->error(__('Please Activate your account first.'));

                            }

                           }
                           $this->Flash->error(__('Unsuccessful Login'));
                        
                    } 
                        
        }

    public function signup()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
        
            $userTable = TableRegistry::get('Users');
            $user = $userTable->newEntity(); 
            $user = $this->Users->patchEntity($user, $this->request->getData());
            //vid
            $myemail = $this->request->getData('email');
            $mytoken = Security::hash(Security::randomBytes(32));
            $user->email = $myemail;
            $user->token =$mytoken;
            // upload
            // if (!empty($this->request->getData['Users']['upload']['name'])) {
            //     $file = $this->request->getData['Users']['upload'];

            //     $ext = substr(strtolower($file['name'], '.'), 1);
            //     $arr_ext = array('jpg','png', 'jpeg');

            //     if (in_array($ext, $arr_extr)) {
            //         move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/uploads' . $file['name']);

            //         $this->getData['Users']['image'] = $file['name'];
            //     }
            // }
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved. Please Check your email for confirmation.'));
                Email::configTransport('mailtrap', [
                  'host' => 'smtp.mailtrap.io',
                  'port' => 2525,
                  'username' => '0494e6d2e5969c',
                  'password' => 'c75b80375719fe',
                  'className' => 'Smtp'
                ]);

                $email = new Email('default');
                $email->transport('mailtrap');
                $email->emailFormat('html');
                $email->from('testmail@gmail.com', 'Admin');
                $email->subject('Please Confirm your email for account activation');
                $email->to($myemail);
                $email->send('hello '.$myemail.'<br/> Pease confirm your email link below<br/><a href="http://localhost:8765/users/verification/'.$mytoken.'">Verification Email</a><br/> Thank you for joing us');

                return $this->redirect(['action' => 'login']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    public function upload(){
        $this->Flash->success(__('The user has been saved. Please Check your email for confirmation.'));
    }

    public function verification($token){
        $userTable = TableRegistry::get('Users');
        $verify = $userTable->find('all')->where(['token'=>$token])->first();
        $verify->is_activated = '1';
        $userTable->save($verify);

    }



    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */

    public function forgotPassword(){
        //later
    }
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Profile has been successfully edited.'));

                return $this->redirect(['controller'=> 'Posts', 'action' => 'profile',$id]);
            }
            $this->Flash->error(__(' Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    
    public function logout(){
        
        $this->Flash->success(__("Successfully Logged out"));  
        return $this->redirect($this->Auth->logout());
    }


}

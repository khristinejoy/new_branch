<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Network\Exception\InternalErrorException;
use Cake\Utility\Text;
use Cake\ORM\TableRegistry;

/**
 * 
 */
class UploadComponent extends Component
{	
	public $max_files = 1;
	
	function send($data)
	{
	if (!empty($data)) {
			if (count($data) > $this->max_files) {
				throw new Exception("Error Processing Request. Max number files accepted is {$this->max_files}", 1);
				
						foreach ($data as $file) {
							$filename = $file['image'];
							$file_tmp_name = $file['tmp_name'];
							$dir = WWW.ROOT.'img'.DS.'path';
							$allowed = array('png','jpg','jpeg');
							if (!in_array(substr(strrchr($filename, '.'), 1), $allowed)) {
								throw new Exception("Error Processing Request", 1);
								
								# code...
							}elseif (is_uploaded_file($file_tmp_name)) {
								$filename = Text::uuid().'-'.$filename;

								$filedb = TableRegistry::get('Users');
								$entity = $filedb->newEntity();
								$entity->image = $filename;
								$filedb->save($entity);

								move_uploaded_file($file_tmp_name, $dir.DS.$filename);
							}

						}
					}		
		}
	}
}

?>
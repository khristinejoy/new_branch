<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Retweets Controller
 *
 * @property \App\Model\Table\RetweetsTable $Retweets
 *
 * @method \App\Model\Entity\Retweet[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RetweetsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Posts']
        ];
        $retweets = $this->paginate($this->Retweets);

        $this->set(compact('retweets'));
    }

    /**
     * View method
     *
     * @param string|null $id Retweet id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $retweet = $this->Retweets->get($id, [
            'contain' => ['Users', 'Posts']
        ]);

        $this->set('retweet', $retweet);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $retweet = $this->Retweets->newEntity();
        if ($this->request->is('post')) {
            $retweet = $this->Retweets->patchEntity($retweet, $this->request->getData());
            if ($this->Retweets->save($retweet)) {
                $this->Flash->success(__('The retweet has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The retweet could not be saved. Please, try again.'));
        }
        $users = $this->Retweets->Users->find('list', ['limit' => 200]);
        $posts = $this->Retweets->Posts->find('list', ['limit' => 200]);
        $this->set(compact('retweet', 'users', 'posts'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Retweet id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $retweet = $this->Retweets->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $retweet = $this->Retweets->patchEntity($retweet, $this->request->getData());
            if ($this->Retweets->save($retweet)) {
                $this->Flash->success(__('The retweet has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The retweet could not be saved. Please, try again.'));
        }
        $users = $this->Retweets->Users->find('list', ['limit' => 200]);
        $posts = $this->Retweets->Posts->find('list', ['limit' => 200]);
        $this->set(compact('retweet', 'users', 'posts'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Retweet id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $retweet = $this->Retweets->get($id);
        if ($this->Retweets->delete($retweet)) {
            $this->Flash->success(__('The retweet has been deleted.'));
        } else {
            $this->Flash->error(__('The retweet could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
